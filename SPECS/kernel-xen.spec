%define kvmajor 5.4
%define kvminor 184
%define pkg_release 3
%define KRELEASE %{kvmajor}.%{kvminor}-%{release}.%{_target_cpu}

Name: kernel-xen
Version: %{kvmajor}.%{kvminor}
Release: %{pkg_release}.el%{rhel}xen
License: GPL
Group: System Environment/Kernel
URL: http://www.kernel.org
Packager: Steven Haigh <netwiz@crc.id.au>
Summary: The Linux Kernel
Vendor: The Linux Community
Provides: kernel-xen = %{version}-%{release}
Provides: kernel = %{version}-%{release}
Requires: kernel-xen-firmware virt-what
BuildRequires: git bc yum-utils openssl openssl-devel
%if 0%{rhel} == 6
BuildRequires: net-tools
%else
BuildRequires: hostname
%endif
BuildRequires: elfutils-libelf-devel
BuildRequires: bison flex
%if 0%{rhel} >= 8
BuildRequires: rsync
%undefine __brp_mangle_shebangs
%global __python %{__python3}
BuildRequires: python36 python36-devel
%endif
BuildRoot: %_topdir/BUILDROOT
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}

Source0: https://www.kernel.org/pub/linux/kernel/v5.x/linux-%{kvmajor}.%{kvminor}.tar.xz
Source1: config-%{kvmajor}.x86_64

Patch0: el6-no-fallthough-warnings.patch

%description
The Linux Kernel, the operating system core itself

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Obsoletes: glibc-kernheaders
Obsoletes: kernel-headers
Provides: glibc-kernheaders = 3.0-46
Provides: kernel-headers = %{version}-%{release}
Provides: kernel-xen-headers = %{version}-%{release}
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package firmware
Summary: Firmware files used by the Linux kernel
Group: Development/System
Provides: kernel-xen-firmware = %{version}-%{release}
%if 0%{rhel} == 6
Requires: kernel-firmware
%endif
%if 0%{rhel} >= 7
Requires: linux-firmware
%endif
%description firmware
Kernel-firmware includes firmware files required for some devices to
operate.

%package devel
Summary: Development package for building kernel modules to match the kernel
Group: System Environment/Kernel
Obsoletes: kernel-devel
Provides: kernel-xen-devel = %{version}-%{release}
Provides: kernel-devel = %{version}-%{release}
%description devel
This package provides kernel headers and makefiles sufficient to build modules
against the kernel package.

%prep
%setup -q -n linux-%{kvmajor}.%{kvminor}

%if 0%{rhel} == 6
%patch0 -p1
%endif

make mrproper
cat $RPM_SOURCE_DIR/config-%{kvmajor}.%{_target_cpu} >> .config

%build
perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}.%{_target_cpu}/" Makefile

make olddefconfig
make %{?_smp_mflags} bzImage
make %{?_smp_mflags} modules

%install
rm -rf $RPM_BUILD_ROOT

## Create directory structure.
mkdir -p $RPM_BUILD_ROOT/boot
touch $RPM_BUILD_ROOT/boot/initramfs-%{KRELEASE}.img
mkdir -p $RPM_BUILD_ROOT/lib/modules
mkdir -p $RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}
mkdir -p $RPM_BUILD_ROOT/usr

# Copy across all our modules.
make %{?_smp_mflags} INSTALL_MOD_PATH=$RPM_BUILD_ROOT INSTALL_FW_PATH=$RPM_BUILD_ROOT/lib/firmware/%{KRELEASE} INSTALL_MOD_STRIP=1 KBUILD_SRC= modules_install

## Pull down all the firmware for kernel-xen-firmware package
echo "Cloning linux-firmware Git repo for kernel-xen-firmware package..."
git clone --depth=1 git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
if [ "$?" -ne "0" ]; then
	echo "ERROR: Problem cloning git repo - exit code $?"
	exit 1
fi

## Copy ONLY firmware that is used by this kernel
for MODULE in $(find $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/ -name *.ko); do
	for FIRMWARE in $(modinfo -F firmware $MODULE); do
		FIRMWARE_PATH=$(dirname "$RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}/$FIRMWARE")
		if [ ! -d "$FIRMWARE_PATH" ]; then
			mkdir -p "$FIRMWARE_PATH"
		fi
		if [ -f "linux-firmware/$FIRMWARE" ]; then
			cp linux-firmware/$FIRMWARE $RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}/$FIRMWARE
		fi
	done
done

## Get a list of all firmware files that already exist in the distro packages and remove them.
repoquery -q -a -l --disablerepo kernel-xen --disablerepo kernel-xen-testing > $RPM_BUILD_ROOT/filelist.txt
if [ "$?" -ne "0" ]; then
	echo "ERROR: repoquery returned exit code $?"
	exit 1
fi
for file in `cat ${RPM_BUILD_ROOT}/filelist.txt | grep -E '^(/usr)?/lib/firmware/'`; do
	file=`echo $file | sed "s/^\/usr//g"`
	file=`echo $file | sed "s/^\/lib\/firmware\///g"`
	if [ -f $RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}/${file} ]; then
		rm -f $RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}/${file}
	fi
done
rm -f "${RPM_BUILD_ROOT}/filelist.txt"

## Clean up empty directories
#find $RPM_BUILD_ROOT/lib/firmware/%{KRELEASE}/* -empty -delete

# Install kernel headers
make %{?_smp_mflags} ARCH=x86 INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_install

# Do headers_check but don't die if it fails.
make %{?_smp_mflags} ARCH=x86 INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_check \
     > hdrwarnings.txt || :
if grep -q exist hdrwarnings.txt; then
   sed s:^$RPM_BUILD_ROOT/usr/include/:: hdrwarnings.txt
   # Temporarily cause a build failure if header inconsistencies.
   # exit 1
fi

find $RPM_BUILD_ROOT/usr/include \
     \( -name .install -o -name .check -o \
        -name ..install.cmd -o -name ..check.cmd \) | xargs rm -f

# glibc provides scsi headers for itself, for now
rm -rf $RPM_BUILD_ROOT/usr/include/scsi
rm -f $RPM_BUILD_ROOT/usr/include/asm*/atomic.h
rm -f $RPM_BUILD_ROOT/usr/include/asm*/io.h
rm -f $RPM_BUILD_ROOT/usr/include/asm*/irq.h

# Manipulate the files how we want them.
cp arch/x86/boot/bzImage $RPM_BUILD_ROOT/boot/vmlinuz-%{KRELEASE}
cp System.map $RPM_BUILD_ROOT/boot/System.map-%{KRELEASE}
cp .config $RPM_BUILD_ROOT/boot/config-%{KRELEASE}
gzip -c9 < Module.symvers > $RPM_BUILD_ROOT/boot/symvers-%{KRELEASE}.gz

# remove files that will be auto generated by depmod at rpm -i time
rm -f $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/modules.*.bin
rm -f $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/build
rm -f $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/source

## Gather up all the files we need for our devel package...
mkdir -p $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
cp --parents `find  -type f -name "Makefile*" -o -name "Kconfig*"` $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
cp Module.symvers $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
cp System.map $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/

# then drop all but the needed Makefiles/Kconfig files
rm -rf $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/Documentation
rm -rf $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/scripts
rm -rf $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/include

## Recopy stuff we probably do need.
cp .config $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
cp -a scripts $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
if [ -d arch/x86/scripts ]; then
	cp -a arch/x86/scripts $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/arch/x86 || :
fi
if [ -f arch/x86/*lds ]; then
	cp -a arch/x86/*lds $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/arch/x86/ || :
fi
rm -f $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/scripts/*.o
rm -f $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/scripts/*/*.o
if [ -d arch/x86/include ]; then
	cp -a --parents arch/x86/include $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/
fi
mkdir -p $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/include
cd include
for i in *; do
	if [ ! -d $i ]; then continue; fi
	cp -a $i $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/include
done

# Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
cp $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/.config $RPM_BUILD_ROOT/usr/src/kernels/%{KRELEASE}/include/config/auto.conf

# Create links to kernel source....
ln -s /usr/src/kernels/%{KRELEASE} $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/build
ln -s /usr/src/kernels/%{KRELEASE} $RPM_BUILD_ROOT/lib/modules/%{KRELEASE}/source

%clean
rm -rf $RPM_BUILD_ROOT

%post
# Add kernel-xen to the list of packages that allow multiple installs
# so we don't nuke working kernels on an upgrade. That would be bad.
if ! grep -q installonlypkgs /etc/yum.conf; then
	sed -i --follow-symlinks 's/\[main]/[main]\ninstallonlypkgs=kernel kernel-xen kernel-smp kernel-bigmem kernel-enterprise kernel-debug/g' /etc/yum.conf
	echo "Added kernel-xen to 'installonlypkgs' line in /etc/yum.conf!"
fi

%if 0%{rhel} == 6
if [ -x /sbin/new-kernel-pkg ]; then
	/sbin/new-kernel-pkg --package kernel --mkinitrd --dracut --depmod --make-default --install %{KRELEASE} || exit $?

	## Check that xen.gz exits, if so, add hypervisor lines, if not, assume we're a DomU or bare metal install.
	if [ -h "/boot/xen.gz" ]; then
		GRUB_CONF=$(readlink -n -e /etc/grub.conf 2>/dev/null)
		if [ -z $GRUB_CONF ]; then
			if [ -f "/boot/grub/grub.conf" ]; then
				GRUB_CONF="/boot/grub/grub.conf"
			else
				echo "No valid grub.conf found. You'll need to fix this manually!"
			fi
		else
			HYPERVISOR=`grep -m1 xen.gz $GRUB_CONF`
			if [[ -z $HYPERVISOR ]]; then
       		 		## We haven't found an existing hypervisor. Find where xen.gz is and add the defaults.
				echo "No existing Xen install found. Using defaults."

				## Look for /boot partition, otherwise assume relative to /
				if `grep -q "/boot" /proc/mounts`; then
					XEN_GZ="/xen.gz"
				else
					XEN_GZ="/boot/xen.gz"
				fi
				HYPERVISOR="	kernel $XEN_GZ dom0_mem=1024M cpufreq=xen dom0_max_vcpus=1 dom0_vcpus_pin"
			fi
			KERNEL=`grep -m1 vmlinuz-%{KRELEASE} $GRUB_CONF`
			INITRAMFS=`grep -m1 initramfs-%{KRELEASE} $GRUB_CONF`
			if [[ -z "$KERNEL" || -z "$INITRAMFS" ]]; then
				echo "ERROR: Something unexpected was found in /etc/grub.conf. Please edit manually."
			else
				KERNEL_NEW=$( echo "$KERNEL" | sed 's|kernel|module|' )
				INITRAMFS_NEW=$( echo "$INITRAMFS" | sed 's|initrd|module|' )
				sed -i --follow-symlinks "s|$KERNEL|$HYPERVISOR\n$KERNEL_NEW|" $GRUB_CONF
				sed -i --follow-symlinks "s|$INITRAMFS|$INITRAMFS_NEW|" $GRUB_CONF
			fi
		fi
	fi
fi
%endif

%if 0%{rhel} >= 7
## Create the initramfs.
if [ -x /sbin/new-kernel-pkg ]; then
	/sbin/new-kernel-pkg --package kernel --mkinitrd --dracut --depmod --make-default --install %{KRELEASE} || exit $?
fi
if [ -x /bin/kernel-install ]; then
  /bin/kernel-install add %{KRELEASE} /boot/vmlinuz
fi

## Check what context we're running in:
# xen or xen-domU= PV DomU
# xen-dom0 = Xen Dom0
# xen-hvm = Xen HVM
# nil output = raw hardware.
PLATFORM=`/usr/sbin/virt-what | tail -n1`
if [ $? == 0 ]; then
	case $PLATFORM in
		"")
			## Find the grub2 config...
			if [ -f "/boot/efi/EFI/redhat/grub.cfg" ]; then
				GRUB2_CONF="/boot/efi/EFI/redhat/grub.cfg"
			elif [ -f "/boot/efi/EFI/centos/grub.cfg" ]; then
				GRUB2_CONF="/boot/efi/EFI/centos/grub.cfg"
			elif [ -f "/boot/grub2/grub.cfg" ]; then
				GRUB2_CONF="/boot/grub2/grub.cfg"
			fi

			## We're running on bare metal - so check to see if the hypervisor exists, and if so, set it up.
			if [ -f "/boot/xen.gz" ]; then
				## Check for and set up grub2.
				if [ -f "/etc/default/grub" ]; then
					if ! grep -q "GRUB_CMDLINE_XEN" "/etc/default/grub"; then
						echo 'GRUB_CMDLINE_XEN="dom0_mem=1024M cpufreq=xen dom0_max_vcpus=1 dom0_vcpus_pin console=tty0 console=com1 com1=115200,8n1"' >> "/etc/default/grub"
					fi
				fi

				## Set the default grub2 boot option to select Xen - as this is an upgrade.
				/sbin/grub2-set-default "`grep ^menuentry $GRUB2_CONF | cut -d\' -f2 | grep Xen`"
				# (vim formatting barfs on the above line - make it happy here with a ' :)
			fi

			## Finally, rebuild the config file.
			/sbin/grub2-mkconfig -o "$GRUB2_CONF" >& /dev/null
			;;

		"xen-dom0")
			## Running as a Xen Dom0
			if [ -f "/etc/default/grub" ]; then
				## Find the grub2 config...
				if [ -f "/boot/efi/EFI/redhat/grub.cfg" ]; then
					GRUB2_CONF="/boot/efi/EFI/redhat/grub.cfg"
				elif [ -f "/boot/efi/EFI/centos/grub.cfg" ]; then
					GRUB2_CONF="/boot/efi/EFI/centos/grub.cfg"
				elif [ -f "/boot/grub2/grub.cfg" ]; then
					GRUB2_CONF="/boot/grub2/grub.cfg"
				fi

				## If BLSCFG is enabled, disable it.
				if grep -q '^GRUB_ENABLE_BLSCFG="*true"*\s*$' /etc/default/grub; then
					sed -i 's/^GRUB_ENABLE_BLSCFG=.*/GRUB_ENABLE_BLSCFG=false/' /etc/default/grub
				fi

				if [ ! -z $GRUB2_CONF ]; then
					/sbin/grub2-mkconfig -o "$GRUB2_CONF" >& /dev/null

					## Set the default grub2 boot option to select Xen - as this is an upgrade.
					/sbin/grub2-set-default "`grep ^menuentry $GRUB2_CONF | cut -d\' -f2 | grep Xen`"
					# (vim formatting barfs on the above line - make it happy here with a ' :)

					## Bugfix
					## When the default was set, grub errors for stupid reasons before booting.
					## To fix this, we re-generate the boot menu (again) and its happy.
					/sbin/grub2-mkconfig -o "$GRUB2_CONF" >& /dev/null
				else
					echo "Running as a Xen Dom0 under EL7 - but no grub2 config file found. Please report a bug."
				fi
			fi
			;;

		"xen"|"xen-hvm"|"xen-domU")
			# Xen 4.6 PV-GRUB now supports grub2 boot configs. If /boot/grub2 exists, use the OS tools, otherwise
			# fall back to creating an old school /boot/grub/menu.lst.
			if [ -d "/boot/grub2" ]; then
				## Grub2 seems to be installed, use the OS tools for installation.
				if [ ! -f "/etc/default/grub" ]; then
					echo 'GRUB_TIMEOUT=5' > /etc/default/grub
					echo 'GRUB_DEFAULT=saved' >> /etc/default/grub
					echo 'GRUB_DISABLE_SUBMENU=true' >> /etc/default/grub
					echo 'GRUB_TERMINAL_OUTPUT="console"' >> /etc/default/grub
					echo 'GRUB_CMDLINE_LINUX="quiet"' >> /etc/default/grub
					echo 'GRUB_DISABLE_RECOVERY="true"' >> /etc/default/grub
				fi

				## If BLSCFG is enabled, disable it.
				if grep -q '^GRUB_ENABLE_BLSCFG="*true"*\s*$' /etc/default/grub; then
					sed -i 's/^GRUB_ENABLE_BLSCFG=.*/GRUB_ENABLE_BLSCFG=false/' /etc/default/grub
				fi

				/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg >& /dev/null
			else
				## No grub2 installation. Create an old school /boot/grub/menu.lst
				if [ ! -d "/boot/grub" ]; then
					mkdir -p /boot/grub
				fi
				GRUB_CONF="/boot/grub/grub.conf"
				if [ -f "/boot/grub/grub.cfg" ]; then
					rm -f "/boot/grub/grub.cfg"
				fi

				## See if we have a separate boot partition.
				mount | grep "/boot" >& /dev/null
				if [ $? == 0 ]; then
					## grep returned 0 - found a match
					BOOT_PREFIX=""
				else
					## grep returned 1 - no matches
					BOOT_PREFIX="/boot"
				fi

				## Find the root partition and make a correct root=/dev/blah entry.
				ROOT=`/bin/df / | tail -n1 | awk '{ print $1 }'`

				## grub header:
				echo "#boot=/dev/xvda" > $GRUB_CONF
				echo "default=0" >> $GRUB_CONF
				echo "timeout=1" >> $GRUB_CONF
				echo "" >> $GRUB_CONF

				## Loop for each kernel-xen package installed and create a grub entry.
				for kernel in `rpm -q kernel-xen --queryformat='%{VERSION}-%{RELEASE}.%{ARCH}\n' | sort -r`; do
				        echo "title `cat /etc/redhat-release` ($kernel)" >> $GRUB_CONF
				        echo "root (hd0)" >> $GRUB_CONF
				        echo "kernel $BOOT_PREFIX/vmlinuz-$kernel ro root=$ROOT" >> $GRUB_CONF
				        echo "initrd $BOOT_PREFIX/initramfs-$kernel.img" >> $GRUB_CONF
				        echo "" >> $GRUB_CONF
				done
			fi
			;;

		*)
			# WHAAAA?
			echo "virt-what said we're running on $PLATFORM, but I don't know what this is. Please file a bug report"
			;;
	esac
else
	echo "virt-what produced a non-zero exitcode. Please report this bug."
fi
%endif

if [ -x /sbin/weak-modules ]
then
	/sbin/weak-modules --add-kernel %{KRELEASE} || exit $?
fi

%preun
if [ -x /sbin/new-kernel-pkg ]; then
	/sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KRELEASE} || exit $?
fi
if [ -x /bin/kernel-install ]; then
  /bin/kernel-install remove %{KRELEASE}
fi
if [ -x /sbin/weak-modules ]; then
	/sbin/weak-modules --remove-kernel %{KRELEASE} || exit $?
fi

%postun
%if 0%{rhel} >= 7
## Regenerate the grub config file in a DomU for kernels still installed.
PLATFORM=`/usr/sbin/virt-what | tail -n1`
if [ $? == 0 ]; then
	case $PLATFORM in
		"xen"|"xen-hvm"|"xen-domU"|"xen-dom0")
			# Xen 4.6 PV-GRUB now supports grub2 boot configs. If /boot/grub2 exists, use the OS tools, otherwise
			# fall back to creating an old school /boot/grub/menu.lst.
			if [ -d "/boot/grub2" ]; then
				## Grub2 seems to be installed, use the OS tools for installation.
				if [ ! -f "/etc/default/grub" ]; then
					echo 'GRUB_TIMEOUT=5' > /etc/default/grub
					echo 'GRUB_DEFAULT=saved' >> /etc/default/grub
					echo 'GRUB_DISABLE_SUBMENU=true' >> /etc/default/grub
					echo 'GRUB_TERMINAL_OUTPUT="console"' >> /etc/default/grub
					echo 'GRUB_CMDLINE_LINUX="quiet"' >> /etc/default/grub
					echo 'GRUB_DISABLE_RECOVERY="true"' >> /etc/default/grub
					echo 'GRUB_ENABLE_BLSCFG=false' >> /etc/default/grub
				fi

				## If BLSCFG is enabled, disable it.
				if grep -q '^GRUB_ENABLE_BLSCFG="*true"*\s*$' /etc/default/grub; then
					sed -i 's/^GRUB_ENABLE_BLSCFG=.*/GRUB_ENABLE_BLSCFG=false/' /etc/default/grub
				fi

				/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg >& /dev/null
			else
				## No grub2 installation. Create an old school /boot/grub/menu.lst
				if [ ! -d "/boot/grub" ]; then
					mkdir -p /boot/grub
				fi
				GRUB_CONF="/boot/grub/grub.conf"
				if [ -f "/boot/grub/grub.cfg" ]; then
					rm -f "/boot/grub/grub.cfg"
				fi

				## See if we have a separate boot partition.
				mount | grep "/boot" >& /dev/null
				if [ $? == 0 ]; then
					## grep returned 0 - found a match
					BOOT_PREFIX=""
				else
					## grep returned 1 - no matches
					BOOT_PREFIX="/boot"
				fi

				## Find the root partition and make a correct root=/dev/blah entry.
				ROOT=`/bin/df / | tail -n1 | awk '{ print $1 }'`

				## grub header:
				echo "#boot=/dev/xvda" > $GRUB_CONF
				echo "default=0" >> $GRUB_CONF
				echo "timeout=1" >> $GRUB_CONF
				echo "" >> $GRUB_CONF

				## Loop for each kernel-xen package installed and create a grub entry.
				for kernel in `rpm -q kernel-xen --queryformat='%{VERSION}-%{RELEASE}.%{ARCH}\n' | sort -r`; do
				        echo "title `cat /etc/redhat-release` ($kernel)" >> $GRUB_CONF
				        echo "root (hd0)" >> $GRUB_CONF
			        	echo "kernel $BOOT_PREFIX/vmlinuz-$kernel ro root=$ROOT" >> $GRUB_CONF
				        echo "initrd $BOOT_PREFIX/initramfs-$kernel.img" >> $GRUB_CONF
				        echo "" >> $GRUB_CONF
				done
			fi
			;;
	esac
fi
%endif

%files
%defattr (-, root, root)
/boot/vmlinuz-%{KRELEASE}
/boot/System.map-%{KRELEASE}
/boot/config-%{KRELEASE}
/boot/symvers-%{KRELEASE}.gz
%ghost /boot/initramfs-%{KRELEASE}.img
%dir /lib/modules/%{KRELEASE}
/lib/modules/%{KRELEASE}/kernel
/lib/modules/%{KRELEASE}/modules.*
/lib/modules/%{KRELEASE}/build
/lib/modules/%{KRELEASE}/source
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.alias
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.alias.bin
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.dep
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.dep.bin
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.devname
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.softdep
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.symbols
%attr(644, root, root) %ghost /lib/modules/%{KRELEASE}/modules.symbols.bin

%files headers
%defattr(-,root,root)
/usr/include

%files firmware
%defattr(-,root,root)
/lib/firmware/%{KRELEASE}

%files devel
%defattr(-,root,root)
/usr/src/kernels/%{KRELEASE}

%changelog
* Sat Mar 12 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.184-1
- Update to 5.4.184

* Wed Mar 09 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.183-1
- Update to 5.4.183

* Thu Mar 03 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.182-1
- Update to 5.4.182

* Thu Feb 24 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.181-1
- Update to 5.4.181

* Thu Feb 17 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.180-1
- Update to 5.4.180

* Sat Feb 12 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.179-1
- Update to 5.4.179

* Wed Feb 09 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.178-1
- Update to 5.4.178

* Sun Feb 06 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.177-1
- Update to 5.4.177

* Wed Feb 02 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.176-1
- Update to 5.4.176

* Sun Jan 30 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.175-1
- Update to 5.4.175

* Fri Jan 28 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.174-1
- Update to 5.4.174

* Fri Jan 21 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.173-1
- Update to 5.4.173

* Mon Jan 17 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.172-1
- Update to 5.4.172

* Wed Jan 12 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.171-1
- Update to 5.4.171

* Thu Jan 06 2022 Steven Haigh <netwiz@crc.id.au> - 5.4.170-1
- Update to 5.4.170

* Thu Dec 30 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.169-1
- Update to 5.4.169

* Thu Dec 23 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.168-1
- Update to 5.4.168

* Sat Dec 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.167-1
- Update to 5.4.167

* Fri Dec 17 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.166-1
- Update to 5.4.166

* Wed Dec 15 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.165-1
- Update to 5.4.165

* Thu Dec 09 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.164-1
- Update to 5.4.164

* Thu Dec 02 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.163-1
- Update to 5.4.163

* Sat Nov 27 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.162-1
- Update to 5.4.162

* Mon Nov 22 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.161-1
- Update to 5.4.161

* Thu Nov 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.160-1
- Update to 5.4.160

* Sat Nov 13 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.159-1
- Update to 5.4.159

* Sun Nov 07 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.158-1
- Update to 5.4.158

* Wed Nov 03 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.157-1
- Update to 5.4.157

* Thu Oct 28 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.156-1
- Update to 5.4.156

* Thu Oct 21 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.155-1
- Update to 5.4.155

* Mon Oct 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.154-1
- Update to 5.4.154

* Thu Oct 14 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.153-1
- Update to 5.4.153

* Sun Oct 10 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.152-1
- Update to 5.4.152

* Thu Oct 07 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.151-1
- Update to 5.4.151

* Fri Oct 01 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.150-1
- Update to 5.4.150

* Mon Sep 27 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.149-1
- Update to 5.4.149

* Thu Sep 23 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.148-1
- Update to 5.4.148

* Fri Sep 17 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.147-1
- Update to 5.4.147

* Thu Sep 16 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.146-1
- Update to 5.4.146

* Sun Sep 12 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.145-1
- Update to 5.4.145

* Sat Sep 04 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.144-1
- Update to 5.4.144

* Fri Aug 27 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.143-1
- Update to 5.4.143

* Wed Aug 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.142-1
- Update to 5.4.142

* Mon Aug 16 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.141-1
- Update to 5.4.141

* Fri Aug 13 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.140-1
- Update to 5.4.140

* Sun Aug 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.139-1
- Update to 5.4.139

* Thu Aug 05 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.138-1
- Update to 5.4.138

* Sat Jul 31 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.137-1
- Update to 5.4.137

* Thu Jul 29 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.136-1
- Update to 5.4.136

* Mon Jul 26 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.135-1
- Update to 5.4.135

* Wed Jul 21 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.134-1
- Update to 5.4.134

* Mon Jul 19 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.133-1
- Update to 5.4.133

* Thu Jul 15 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.132-1
- Update to 5.4.132

* Mon Jul 12 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.131-1
- Update to 5.4.131

* Thu Jul 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.130-1
- Update to 5.4.130

* Thu Jul 01 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.129-1
- Update to 5.4.129

* Thu Jun 24 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.128-1
- Update to 5.4.128

* Sat Jun 19 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.127-1
- Update to 5.4.127

* Thu Jun 17 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.126-1
- Update to 5.4.126

* Fri Jun 11 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.125-1
- Update to 5.4.125

* Thu Jun 03 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.124-1
- Update to 5.4.124

* Sat May 29 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.123-1
- Update to 5.4.123

* Thu May 27 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.122-1
- Update to 5.4.122

* Sun May 23 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.121-1
- Update to 5.4.121

* Thu May 20 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.120-1
- Update to 5.4.120

* Sat May 15 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.119-1
- Update to 5.4.119

* Wed May 12 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.118-1
- Update to 5.4.118

* Sat May 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.117-1
- Update to 5.4.117

* Mon May 03 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.116-1
- Update to 5.4.116

* Thu Apr 29 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.115-1
- Update to 5.4.115

* Thu Apr 22 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.114-1
- Update to 5.4.114

* Sat Apr 17 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.113-1
- Update to 5.4.113

* Wed Apr 14 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.112-1
- Update to 5.4.112

* Sun Apr 11 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.111-1
- Update to 5.4.111

* Thu Apr 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.110-1
- Update to 5.4.110

* Wed Mar 31 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.109-1
- Update to 5.4.109

* Thu Mar 25 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.108-1
- Update to 5.4.108

* Sun Mar 21 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.107-1
- Update to 5.4.107

* Thu Mar 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.106-1
- Update to 5.4.106

* Fri Mar 12 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.105-1
- Update to 5.4.105

* Wed Mar 10 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.104-1
- Update to 5.4.104

* Mon Mar 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.103-1
- Update to 5.4.103

* Fri Mar 05 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.102-1
- Update to 5.4.102

* Sat Feb 27 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.101-1
- Update to 5.4.101

* Wed Feb 24 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.100-1
- Update to 5.4.100

* Thu Feb 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.99-1
- Update to 5.4.99

* Sun Feb 14 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.98-1
- Update to 5.4.98

* Thu Feb 11 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.97-1
- Update to 5.4.97

* Mon Feb 08 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.96-1
- Update to 5.4.96

* Thu Feb 04 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.95-1
- Update to 5.4.95

* Sun Jan 31 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.94-1
- Update to 5.4.94

* Thu Jan 28 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.93-1
- Update to 5.4.93

* Sun Jan 24 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.92-1
- Update to 5.4.92

* Wed Jan 20 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.91-1
- Update to 5.4.91

* Mon Jan 18 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.90-1
- Update to 5.4.90

* Wed Jan 13 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.89-1
- Update to 5.4.89

* Sun Jan 10 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.88-1
- Update to 5.4.88

* Thu Jan 07 2021 Steven Haigh <netwiz@crc.id.au> - 5.4.87-1
- Update to 5.4.87

* Thu Dec 31 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.86-1
- Update to 5.4.86

* Tue Dec 22 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.85-1
- Update to 5.4.85

* Thu Dec 17 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.84-1
- Update to 5.4.84

* Sat Dec 12 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.83-1
- Update to 5.4.83

* Wed Dec 09 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.82-1
- Update to 5.4.82

* Thu Dec 03 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.81-1
- Update to 5.4.81

* Wed Nov 25 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.80-1
- Update to 5.4.80

* Mon Nov 23 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.79-1
- Update to 5.4.79

* Thu Nov 19 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.78-1
- Update to 5.4.78

* Thu Nov 12 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.77-1
- Update to 5.4.77

* Wed Nov 11 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.76-1
- Update to 5.4.76

* Fri Nov 06 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.75-1
- Update to 5.4.75

* Mon Nov 02 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.74-1
- Update to 5.4.74

* Fri Oct 30 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.73-1
- Update to 5.4.73

* Sun Oct 18 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.72-1
- Update to 5.4.72

* Thu Oct 15 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.71-1
- Update to 5.4.71

* Wed Oct 07 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.70-1
- Update to 5.4.70

* Fri Oct 02 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.69-1
- Update to 5.4.69

* Sun Sep 27 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.68-1
- Update to 5.4.68

* Thu Sep 24 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.67-1
- Update to 5.4.67

* Fri Sep 18 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.66-1
- Update to 5.4.66

* Sun Sep 13 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.65-1
- Update to 5.4.65

* Thu Sep 10 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.64-1
- Update to 5.4.64

* Sun Sep 06 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.63-1
- Update to 5.4.63

* Fri Sep 04 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.62-1
- Update to 5.4.62

* Thu Aug 27 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.61-1
- Update to 5.4.61

* Sat Aug 22 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.60-1
- Update to 5.4.60

* Wed Aug 19 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.59-1
- Update to 5.4.59

* Wed Aug 12 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.58-1
- Update to 5.4.58

* Fri Aug 07 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.57-1
- Update to 5.4.57

* Thu Aug 06 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.56-1
- Update to 5.4.56

* Sat Aug 01 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.55-1
- Update to 5.4.55

* Thu Jul 30 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.54-1
- Update to 5.4.54

* Wed Jul 22 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.53-1
- Update to 5.4.53

* Thu Jul 16 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.52-1
- Update to 5.4.52

* Thu Jul 09 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.51-1
- Update to 5.4.51

* Thu Jul 02 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.50-1
- Update to 5.4.50

* Thu Jun 25 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.49-1
- Update to 5.4.49

* Mon Jun 22 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.48-1
- Update to 5.4.48

* Thu Jun 18 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.47-1
- Update to 5.4.47

* Thu Jun 11 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.46-1
- Update to 5.4.46

* Mon Jun 08 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.45-1
- Update to 5.4.45

* Wed Jun 03 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.44-1
- Update to 5.4.44

* Thu May 28 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.43-1
- Update to 5.4.43

* Wed May 20 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.42-1
- Update to 5.4.42

* Thu May 14 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.41-1
- Update to 5.4.41

* Mon May 11 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.40-1
- Update to 5.4.40

* Wed May 06 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.39-1
- Update to 5.4.39

* Sun May 03 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.38-1
- Update to 5.4.38

* Sat May 02 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.37-1
- Update to 5.4.37

* Thu Apr 30 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.36-1
- Update to 5.4.36

* Fri Apr 24 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.35-1
- Update to 5.4.35

* Tue Apr 21 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.34-1
- Update to 5.4.34

* Sat Apr 18 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.33-1
- Update to 5.4.33

* Tue Apr 14 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.32-1
- Update to 5.4.32

* Wed Apr 08 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.31-1
- Update to 5.4.31

* Fri Apr 03 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.30-1
- Update to 5.4.30

* Thu Apr 02 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.29-1
- Update to 5.4.29

* Thu Mar 26 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.28-1
- Update to 5.4.28

* Sun Mar 22 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.27-1
- Update to 5.4.27

* Wed Mar 18 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.26-1
- Update to 5.4.26

* Fri Mar 13 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.25-1
- Update to 5.4.25

* Fri Mar 06 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.24-1
- Update to 5.4.24

* Mon Feb 24 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.21-2
- Enable bcache and btrfs support

* Thu Feb 20 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.21-1
- Update to 5.4.21

* Sat Feb 15 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.20-1
- Update to 5.4.20

* Wed Feb 12 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.19-5
- Update to 5.4.19

* Mon Feb 10 2020 Steven Haigh <netwiz@crc.id.au> - 5.4.18-1
- Update to kernel 5.4.18
